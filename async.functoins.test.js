const { todo } = require('./async.functions');

test('todo fetch test', () => {
    expect.assertions(2);
    return todo.then((data) => {
        expect(data.title).toBeDefined()
        expect(data.title).not.toBeNull()
    });
})