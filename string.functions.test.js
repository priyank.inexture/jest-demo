const { reverseString } = require('./string.functions')

test("reverse string of abc is cba", () => {
    expect(reverseString('abc')).toBe('cba');
})
test("reverse string of abc is not cab", () => {
    expect(reverseString('abc')).not.toBe('cab');
})