## Notes

To run tests `npm test`

### Truthiness

- `toBeNull` matches only null
- `toBeUndefined` matches only undefined
- `toBeDefined` is the opposite of toBeUndefined
- `toBeTruthy` matches anything that an if statement treats as true
- `toBeFalsy` matches anything that an if statement treats as false

### Numbers

-  `toBeGreaterThan`
- `toBeGreaterThanOrEqual`
-  `toBeLessThan`
- `toBeLessThanOrEqual`
- `toBe` & `toEqual` are same for numbers

### Strings

- `toMatch` matches regex 

### Arrays & iterables 

- `toContainer` matches contains element

### Exceptions

- `toThrow()` matches if error
- `toThrow(Error)` matches Error
- `toThrow('Error Message')` matches error message
- `toThrow(/RGX/)` matches regex with error message

### Promises 

#### Assertions
``` 
    test('the fetch fails with an error', () => {
        expect.assertions(1); //Number of assertions expected
        return fetchData().catch(e => expect(e).toMatch('error'));
    });
```
#### Resolve promise
``` 
    test('the data is peanut butter', () => {
        return expect(fetchData()).resolves.toBe('peanut butter');
    });
```
#### Reject promise
``` 
    test('the fetch fails with an error', () => {
        return expect(fetchData()).rejects.toMatch('error');
    });
```

#### With async & await

```
    test('the data is peanut butter', async () => {
        const data = await fetchData();
        expect(data).toBe('peanut butter');
    });

    test('the fetch fails with an error', async () => {
        expect.assertions(1);
        try {
            await fetchData();
        } catch (e) {
            expect(e).toMatch('error');
        }
    });
```

### Globals

- `afterAll(fn, timeout)` Runs a function after all the tests in this file have completed
- `afterEach(fn, timeout)` Runs a function after each one of the tests in this file completes.
- `beforeAll(fn, timeout)` Runs a function before any of the tests in this file run. 
- `beforeEach(fn, timeout)` Runs a function before each of the tests in this file runs. 
- `describe(name, fn)` creates a block that groups together several related tests. (can be used to have similar after & before functions)