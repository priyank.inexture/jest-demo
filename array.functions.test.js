const { equalChunckArray } = require('./array.functions');
const { CannotDivideError } = require('./error');
test("[1,2,3,4,5,6,7,8,9],3", () => {
    expect(equalChunckArray([1, 2, 3, 4, 5, 6, 7, 8, 9], 3)).toEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
})
test("[1,2,3,4,5,6,7,8,9],4", () => {
    expect(() => equalChunckArray([1, 2, 3, 4, 5, 6, 7, 8, 9], 4)).toThrow(CannotDivideError);
})