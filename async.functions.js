const axios = require('axios');
const todo = axios.get('https://jsonplaceholder.typicode.com/todos/1').then(response => response.data).catch(error => error)
module.exports = { todo };