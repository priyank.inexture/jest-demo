const { add, sub, div, mul } = require('./arithmetic.functions');

test('Adds 2 + 2 to equals 4', () => {
    expect(add(2, 2)).toBe(4);
})
test('Subs 2 - 2 to equals 0', () => {
    expect(sub(2, 2)).toBe(0);
})
test('Subs 2 - 2 not equals 10', () => {
    expect(sub(2, 2)).not.toBe(10);
})
test('2 * 2 less than 10', () => {
    expect(mul(2, 2)).toBeLessThan(10);
})
test('2.2 * 1 equals 2.2', () => {
    expect(mul(2.2, 1)).toBe(2.2);
})
test('0.3 * 0.2 close to 0.61', () => {
    expect(mul(0.3, 0.2)).toBeCloseTo(0.061)
})
test('0 / 0  NaN', () => {
    expect(div(0, 0)).toBeNaN()
})
test('1 / 0  Infinity', () => {
    expect(div(1, 0)).toBe(Infinity);
})
