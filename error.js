const CannotDivideError = new Error('Cannot Divide into equal chunks');
module.exports = { CannotDivideError }