const { CannotDivideError } = require('./error');
const equalChunckArray = (arr, length) => {
    if (arr.length % length !== 0)
        throw CannotDivideError
    else {
        let result = [];
        const origLength = arr.length;
        for (let index = 0; index < origLength / length; index++) {
            result.push(arr.splice(0, length))
        }
        return result;
    }
}
module.exports = { equalChunckArray };